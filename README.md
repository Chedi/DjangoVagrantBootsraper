# DjangoVagrantBootsraper
Simple bootstraping script for a django full stack vagrant based environment

This project will create a vagrant image (based on fedora 22 cloud) that hosts a full stack django application
with:
- Nginx full https web server
- Postgresql datanase
- Redis cache server
- gunicorn running via supervisor

The vagrant image can be found [here](https://github.com/Chedi/vagrant_f22_django), it's a fedora 22 vagrant 
cloud image coupled with a puppet module to deploy both the necessary servers and the application code from
a git repositoy.

You can check the source of the puppet module [chedi-django](https://github.com/Chedi/django-puppet) or simply
install it from the puppet forge [here](https://forge.puppetlabs.com/chedi/django). The particularity of this 
module is that is will also deploy the django application, setup the virtual environment and perform the 
necessary action to run the application like the database synchronization and the static files collections.

This script will also setup an additional user named developper with the necessary rights to access the 
django application. An alias to the virtual machine will be added to the /etc/hosts in the end of the installation
and if the [sshsf](http://fuse.sourceforge.net/sshfs.html) utility is installed on the system it will mount
the project folder under a local file with the name of the application.

A set of environment variables passed throughout all the installation permit you to customize some aspect of the 
deployment:

 - XANADOU_PATH: 
 
 The path of the application installation inside the vm, if not present it will be created and owned by the 
 nginx user
 
 - XANADOU_SOURCE: 
 
 A public git repository containing you django application, must complay with a set of contraints to be
 installed. The simple application with the script can be found [here](https://github.com/Chedi/test_app).
 
 Notice that the application is expecting somme configuration variable to run correctly. These variable are
 passed in the form of a base64 json encoded dictionnary. If none is passed the default value are set by the 
 puppet facter (which are the same as in this script).
 
 - XANADOU_PYTHON_3:
 
 whether or not you are enabling the python3 for the virtual environment and the django application
 
 - XANADOU_APP_NAME:
 
 the named of the application (keep it space free), this will be used to create the application folder
 and in the various places in the django application.
 
 - XANADOU_SERVER_NAME: 
 
 The vhost server name to be used with nginx and the address to which the host alias will be set. The rules
 of host naming apply here.
 
 - XANADOU_WRITE_REPOSITORY: 
 
 An optional git repository that will be used to push modification from the vm
