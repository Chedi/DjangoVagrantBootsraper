#!/usr/bin/env bash

# This function will check if a given command exists on the system
command_exists () {
  type "$1" &> /dev/null ;
}

# Check if variable is empty
empty () {
    if [ -z ${1+x} ]
    then
        return 0
    else
        return 1
    fi
}

# Forcing the provisioning provider
# libvirt:
#export VAGRANT_DEFAULT_PROVIDER='libvirt'
# virtualbox
#export VAGRANT_DEFAULT_PROVIDER='virtualbox'

echo "Initializing environement variables"

export XANADOU_PATH='/var/django'
export XANADOU_SOURCE='https://github.com/Chedi/test_app.git'
export XANADOU_PYTHON_3='true'
export XANADOU_APP_NAME='test_app'
export XANADOU_SERVER_NAME='www.test-app.dev'
export XANADOU_WRITE_REPOSITORY='git@github.com:Chedi/test_app.git'

# Add your custom variable to be passed to the django application here in json format
read -r -d '' XANADOU_EXTRA_INFO << EOM
{
  "debug"               : true,
  "google_oauth2_key"   : "1014089072090-b9dml6lg4f7kqe56d7qummg01cnvigoi.apps.googleusercontent.com",
  "google_oauth2_secret": "wvdMdxMR6KQpdLHzMM9wOPqZ"
}
EOM

# Encoding the extra information in base64 and removing the spaces and newlines
export XANADOU_EXTRA_INFO=`echo $XANADOU_EXTRA_INFO | openssl enc -base64 | tr -d " \n"`

echo "Checking out the vagrant configuration"

if [ ! -d vagrant_f22_django ]
then
    git clone https://github.com/Chedi/vagrant_f22_django.git
fi

# Starting the provisioning/reload of the virtual machine
echo "Firing the vagrant vm"

cd vagrant_f22_django
vagrant up

provider_name=$(vagrant status --machine-readable | sed -n 's/.\+provider-name,\(.\+\)$/\1/p')

echo "Setup remote developper configuration"

if [ -f ~/.ssh/id_dsa.pub ]
then
    DEVELOPPER_PUBLIC_KEY=$(<~/.ssh/id_dsa.pub)
else
    echo|ssh-keygen -t dsa &> /dev/null
fi

read -r -d '' REMOTE_ACCESS_SETUP << EOM
      sudo useradd developper                                                      &&
      sudo usermod -G nginx developper                                             &&
      sudo su - developper -c "echo|ssh-keygen -t dsa &> /dev/null"                &&
      sudo sh -c "echo developper ALL=NOPASSWD: ALL > /etc/sudoers.d/developper"   &&
      sudo sh -c "echo $DEVELOPPER_PUBLIC_KEY >> ~developper/.ssh/authorized_keys" &&
      sudo chown developper ~developper/.ssh -R
EOM

read -r -d '' UPDATE_WRITE_REPOSITORY <<EOM
      cd $XANADOU_PATH                                      &&
      sudo git remote remove origin                         &&
      sudo git remote add origin $XANADOU_WRITE_REPOSITORY  &&
      sudo chown nginx.nginx .git -R
EOM

if ! empty  $XANADOU_WRITE_REPOSITORY
then
    REMOTE_ACCESS_SETUP="$REMOTE_ACCESS_SETUP && $UPDATE_WRITE_REPOSITORY"
fi

vagrant ssh -c "$REMOTE_ACCESS_SETUP"

# Geting the IP address of the vm and adding it to the hosts file
echo "Getting vm ip address"

while true
do
    vm_ips=$(vagrant ssh -c "ip address | sed -n 's/.*inet \(.\+\)\/.\+eth.\?/\1/p'" | tr -s '\n\r' ',')
    if [[ $vm_ips =~ ^(((([0-9]{1,3}\.){3}[0-9]{1,3}),)+)$ ]]; then
        break
    else
        sleep 2
    fi
done

IFS=', ' read -a vm_ips <<< $vm_ips

echo "checking connectivity of vm ip addresses"

for i in "${vm_ips[@]}"
do
    if ping -c1 -W1 $i &> /dev/null; then
        connectivity_status='OK'
        XANADOU_VM_IP=$i
    else
        connectivity_status='KO'
    fi
    echo "$i : $connectivity_status"
done

if empty $XANADOU_VM_IP
then
    echo "Failed to acquire a valid ip !"
    exit 1
fi

cd ..

#Cleaning up the ssh know hosts list in case of multiple provisioning of the vm
sed -i "/^$XANADOU_VM_IP .\+/d" ~/.ssh/known_hosts

if command_exists 'sshfs'
then
    echo "Mounting the remote application folder locally"
    if [ ! -d $XANADOU_APP_NAME ]; then
       mkdir $XANADOU_APP_NAME
    fi

    mount_path="$(pwd)/$XANADOU_APP_NAME"

    if mount | grep $mount_path > /dev/null; then
        echo "Already mouted, unmounting"
        fusermount -u $mount_path
    else
        sshfs developper@$XANADOU_VM_IP:$XANADOU_PATH "$XANADOU_APP_NAME" \
              -o ssh_command="ssh -o StrictHostKeyChecking=no"
    fi
fi

echo "To access your developper account on the remote machine: ssh developper@$XANADOU_VM_IP"
saved_server_name=$(sed -n "s/^\([0-9.]\+\)\s\($XANADOU_SERVER_NAME\)/\2/p" /etc/hosts)
read saved_server_ip saved_server_name <<< $(sed -n "s/^\([0-9.]\+\)\s\($XANADOU_SERVER_NAME\)/\1 \2/p" /etc/hosts)

read -r -d '' hosts_alias <<EOM

#Vagrant Dev server $XANADOU_SERVER_NAME
$XANADOU_VM_IP $XANADOU_SERVER_NAME

EOM

echo "Adding alias to the vm ip address to the /etc/hosts"
if empty $saved_server_ip && empty $saved_server_name
then
    sudo sh -c "echo '$hosts_alias' >> /etc/hosts"
elif [ $saved_server_ip = $XANADOU_VM_IP ]; then
    echo "Host alias already set, skiping"
else
    echo "You have a defined entry in your /etc/hosts for the $XANADOU_SERVER_NAME, updating"
    sudo sed -i "s/$saved_server_ip $XANADOU_SERVER_NAME/$XANADOU_VM_IP $XANADOU_SERVER_NAME/" /etc/hosts
fi
